import { apiUrls } from "../../config";
import { hashPassword } from "./password";


export function loginUser(user) {
        user.password = hashPassword(user.password);

        user = JSON.stringify(user);

        return fetch(apiUrls.login, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: user
        })
    }