import React from "react";

import PropTypes from 'prop-types';
import { apiUrls } from "../../config";


const UserProfile = (props) => {
    return (
        <div className="card-stacked">
            <div className="card-content">
                <table className="col s12">
                    <tbody className="col s12">
                        <tr className="col s12">
                            <td className="col s6">
                                <img src={`${apiUrls.profileImageDownload}/${props.username}`} alt="" className="circle responsive-img" />
                            </td>
                            <td className="col s6 labels">
                                <div className="row">
                                    <label>{props.username.toUpperCase()}</label>
                                </div>
                                <div className="row">
                                    <label>{props.highscore}</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );
}

UserProfile.propTypes = {
    username: PropTypes.string,
    highscore: PropTypes.number

}

export default UserProfile;